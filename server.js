const express = require('express');
const expressLayouts = require ('express-ejs-layouts');
const ejs= require('ejs');
const mysql = require('mysql');
const bcrypt = require('bcryptjs');
const bodyParser = require('body-parser');
const session = require('express-session');

const app = express();

require('dotenv').config();


const con = mysql.createConnection({
	host : process.env.DB_HOST,
	user : process.env.DB_USER,
	password : process.env.DB_PASSWORD,
	database : process.env.DB_NAME
})

con.connect((err) => {
	if(err){
		console.log(err);
		return;
	}
	console.log('DB connected !')
})



app.use(expressLayouts);
app.set('view engine', 'ejs');

app.use(express.static(`${__dirname}/static`));
//app.use(express.static(`${__dirname}/views`));


app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use(session({
    secret: process.env.SECRET,
    resave: false,
    saveUninitialized: false
 }))                      //////////::::::: SESSION STUFF
 
 
 let does_session_exist = (req, res, next) => {
    if (req.session.user !== undefined) {
        res.redirect('/sign_in');
    } else {
        next();
    }    
};




/////////////////////////////

//  ROUTES    


////////////////////////////



app.get('/',(req,res) => res.render('Landing_Page')
);


app.get('/register',(req,res) => res.render('Register')
);

app.post('/register',(req,res) => {

    let user_alerts = [];

    if(req.body.name ==''|| req.body.firstname ==''|| req.body.mail ==''|| req.body.password ==''){
        user_alerts.push({alert_one: 'Can\'t process request. Missing information. Make sure to fill in all fields'});
    
    }
    else if(req.body.password !== password_check){
        user_alerts.push({alert_two:'Passwords don\'t match.'});
        
    }

    if (user_alerts.length >= 1){
        res.render('Register',{
            user_alerts: user_alerts
        })
    }
    else{
    con.query(`SELECT * FROM Users WHERE email= '${req.body.mail}';`,(err, result, fields)=>{
        if(err){
            console.log(err)
        }else if( result.email== req.body.mail){
            user_alerts.push({alert_three:'E-mail already in use. Make sure you don\'t already have an account'});
        }

    })
    }

    bcrypt.hash(req.body.password, 12, function(err, hash) {
    con.query(`INSERT INTO Users(name, firstname, email, password) VALUES('${req.body.name}','${req.body.firstname}','${req.body.mail}','${hash}');`, (err, result, fields)=>{
        if(err){
            console.log(err)
        }
    })
    });

    res.redirect('/sign_in')
});






app.get('/sign_in',(req,res) => res.render('Sign_in')
);

app.post('/sign_in',(req,res)=>{         /////////////////   incomplete
    
    con.query(`SELECT * FROM Users WHERE email= '${req.body.mail}';`,(err, user, fields) =>{
        if(err){
            console.log(err)
        }else if (user ===[]){
            console.log( "no mail was found in db")
            res.render('Sign_in',{
                alert: 'No such mail exists in our database.'
            })
        }
        else if (user !== []){
            let db_mail = user.email;
            let input_mail = req.body.mail;
            let db_pwd = user.password;
            let input_pwd = req.body.password;     
            
            bcrypt.compare(input_pwd, db_pwd, function(err, success){
                if(success ===true){
                    console.log(successsssssssssss)
                }
            })
        }

    })

    res.redirect('/home')
})

/////////////////////////////

//  ROUTES     PAGES ADMIN .........


////////////////////////////



app.get('/admin',does_session_exist,(req,res)=> res.render('Admin_Panel_Control'));



/////////////////////////////

//  ROUTES     PAGE ADMIN USERS PANEL CONTROL


////////////////////////////

app.get('/admin/usersPC',does_session_exist,(req, res)=> {
    con.query(`SELECT * FROM Users;`,(err, users, fields) => {
        if(err){
            console.log(err);
            return
        }
            console.log(users);
            return users
    })
    con.query(`SELECT MAX(idUsers) AS nb_users FROM Users;`,(err, nb_users, fields) => {
        if(err){
            console.log(err);
            return
        }
            return nb_users
    })
    res.render('Admin_Users_PC',{            
            Users: users,
            Nb_users: nb_users
    })
});

app.post('/admin/usersPC/search',does_session_exist,(req, res)=> {
    con.query(`SELECT * FROM Users WHERE idUsers= '${req.body.user_firstname}' ;`, (err,search_result)=>{
        if(err){
            console.log(err)
        }
        res.render('Admin_Users_PC',{
            Search_results :search_result
        })

    })
});

app.get('/admin/usersPC/kick/:id',does_session_exist,(req,res)=>{
    con.query(`DELETE FROM Users WHERE idUsers = '${req.params.id}';`,(err, user_targeted, fields) => {
        if(err){
            console.log(err);
            return
        }
        res.redirect('/admin/usersPC')
    })
});


/////////////////////////////

//  ROUTES     PAGE ADMIN CHALLENGES PANEL CONTROL


////////////////////////////


app.get('/admin/challengesPC',does_session_exist, (req, res)=> {
    con.query(`SELECT * FROM Challenges;`,(err, challenges, fields) => {
        if(err){
            console.log(err);
            return
        }
        res.render('Admin_Challenges_PC',{
            Challenges: challenges
        });
    })
});

app.get('/admin/challengesPC/remove/:id', (req, res)=> {
    con.query(`DELETE FROM Challenges WHERE idChallenges= '${req.params.id}';`,(err, challenge_targeted, fields)=>{
        if(err){
            console.log(err);
            return
        }
        res.redirect('/admin/challengesPC')
    })
})

///////////////////////////////////              params ds leur gloablite  :id  bon ou idChallenges,,????

app.get('/admin/challengesPC/edit/:id',(req, res)=> {
    con.query(`SELECT * FROM Challenges WHERE idChallenges= '${req.params.id}';`,(err, challenge_targeted, fields)=>{
        if(err){
            console.log(err);
            return
        }
        res.render('Admin_edit_challenge',{
             Challenge_targeted: challenge_targeted[0]
        })
    })

});


app.post('/admin/challengesPC/edit/:id',(req, res)=> {
    con.query(`UPDATE Challenges SET title= '${req.body.title}', description= '${req.body.description}' WHERE idChallenges= '${req.params.id}';`,(err, challenge_targeted, fields)=>{
        if(err){
            console.log(err);
            return
        }
        res.redirect('/admin/challengesPC')
    })
});


app.post('/admin/challengesPC/add',(req, res)=> {
    con.query(`INSERT INTO Challenges (title, description) VALUES('${req.body.title}', '${req.body.description}');`,(err, new_challenge, fields)=>{
        if(err){
            console.log(err);
            return
        }
        res.redirect('/admin/challengesPC')
    })
});



/////////////////////////////

//  ROUTES    CORE OF THE SITE     HOME 


////////////////////////////

/*version   3

app.get('/home',(req,res)=>{
    con.query(`SELECT * FROM Challenges, Tips WHERE Challenges.fk_idTips = Tips.idTips;`, (err, challenges_and_tips, fields) => {
        if(err){
            console.log(err);
            return
        }
        res.render('Home',{
            
           Challenges_and_tips : challenges_and_tips

        })
    })
});

///version 4
*/
app.get('/home',does_session_exist,(req,res)=>{
    con.query(`SELECT * FROM Challenges;`, (err, challenges, fields) => {
        if(err){
            console.log(err);
            return
        }
        return challenges
    })
    con.query(`SELECT * FROM Tips;`, (err, tips, fields)=>{
        if(err){
            console.log(err);
            return
        }
        return tips
    })
    /* version 6         devrait faire avec ça...   peut pas log challengesandtipsdonc sais pas quoi renseigner ds view en terme de donnee abandon
    page cassee de toute maniere  pour le moment ds view tips dissocie de challenge a coz 2 requete separées  pas de correspondance mm si ca marchait
    afficherait pas bon truc 
      si le temps creuser de ce cote la 
    con.query(`SELECT * FROM Challenges INNER JOIN Tips ON Challenges.idChallenges = Tips.fk_idChallenges;`, (err, challengesandtips, fields) => {

    */

    /* ${Challenges[i]} pas bon mais c pas comment chopper la valeur pour chacun sans mettre lien avec un post ou get  */
    con.query(`SELECT COUNT(*) AS PeepsEngaged FROM Activity_Monitor WHERE fk_idChallenges='${Challenges[i]}';`, (err, global_memory, fields)=>{ 
        if(err){
            console.log(err);
            return
        }
        return global_memory
    })

    res.render('Home', {
        Challenges: challenges,
        Tips: tips,
        Global_memory: global_memory
    })
});

app.get("/home/activity_monitor/join/:idChallenges",(req,res)=>{
    con.query(`INSERT INTO Activity_monitor(fk_idUsers, fk_idChallenges, status) VALUES('${req.session}', '${req.params.idChallenges}', "pending");`, (err, newjoining, fields)=>{
        if(err){
            console.log(err);
            return
        }
        res.render('Home',{
            msg:" Thank you for your engagement. You are on your way to accomplish great things! "
        })
    })
})



app.get('/profile/:id',does_session_exist,(req,res)=>{
//prolly not working but oh hell.. i'll try
    con.query(`SELECT am.status AS engagement_status, c.title AS challenge_name, c.description AS challenge_details 
        FROM Challenges AS c INNER JOIN Activity_Monitor AS am 
       ON am.fk_idChallenges = c.idChallenges 
        WHERE am.fk_idUsers = '${req.params.id}';`,(err, global_memory, fields)=>{  
      
        if(err){
            console.log(err)
        }
        return global_memory
    })
    res.render('Profile',{
        Global_memory: global_memory
    })
});

app.post('/profile/activities/edit/:fk_idChallenges',(req,res)=>{
    switch(req.body.status){
        case 'ongoing':
            con.query(`UPDATE Activity_monitor SET status = 'ongoing' WHERE fk_idChallenges = '${req.params.fk_idChallenges}'  ;`, (err, new_status, fields)=>{
                if(err){
                    console.log(err);
                }
                return new_status
            })
            break;
        case 'completed':
            con.query(`UPDATE Activity_monitor SET status = 'completed' WHERE fk_idChallenges = '${req.params.fk_idChallenges}' ;`, (err, new_status, fields)=>{
                if(err){
                     console.log(err);
                }
                return new_status
                })
            break;
            case 'aborted':
                    con.query(`UPDATE Activity_monitor SET status = 'aborted' WHERE fk_idChallenges = '${req.params.fk_idChallenges}' ;`, (err, new_status, fields)=>{
                        if(err){
                            console.log(err);
                        }
                        return new_status
                    })
                    break;

    }
    res.render('Profile',{
        Global_memory :new_status         ////// attentipn     mistake  on views !!! href share profile   fruits test

    })
})



app.listen(process.env.PORT, () => console.log(`listen on ${process.env.PORT}`));